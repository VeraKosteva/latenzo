//***********************************************
// responsiveCollapse menu
//***********************************************
(function ($) {
  $.fn.responsiveCollapse = function (options) {
    var settings = $.extend({
      breakPoint: 992,
      overflowItemText: 'MET BEGINLETTER <b class="caret"></b>'
    }, options);

    var lastDocWidth = $(document).width();

    return this.filter('ul').each(function () {
      var $list = $(this);
      var container = $list.closest('.brand-nav');
      var overflowItem = $('<li class="page-item dropdown d-none"><a href="#" class="page-link dropdown-toggle" data-toggle="dropdown" x-placement="auto-right">' + settings.overflowItemText + '</a><ul class="dropdown-menu dropdown-menu-right"></ul></li>').appendTo($list);
      var overflowList = $(overflowItem).find('ul.dropdown-menu');

      var thisWidth = $list.outerWidth();
      var maxWidth = $(container).width() - $(container).find('.all').width();

      function reset() {
        var child = $(overflowList).children();
        var count = child.length;
        if (count > 0) {
          for (var i = 0; i < count; i++) {
            $(child[i]).insertBefore($list.children(':last-child'));
          }
        }
        $(overflowItem).addClass('d-none');
      }

      function grow() {
        var overflowWidth = $(overflowItem).width();
        var child = $(overflowList).children();
        var count = child.length;

        if (count > 0) {
          for (var i = 0; i < count; i++) {
            // move the first dropdown item to end of list
            $(child[i]).insertBefore($list.children(':last-child'));
            thisWidth = $list.outerWidth();
            if ((i == count - 1 && thisWidth - overflowWidth > maxWidth) || (i != count - 1 && thisWidth > maxWidth)) {
              $(child[i]).prependTo(overflowList);
              break;
            }
          }
        }

        // Hide the overflow item if it has no children
        if ($(overflowList).children().length == 0) {
          $(overflowItem).addClass('d-none');
        }

        return;
      }

      function shrink() {
        var child = $list.children(':not(:first-child):not(:last-child)');
        var count = child.length;
        if (count < 1) return;

        // show the overflow link in case it's been hidden
        $(overflowItem).removeClass('d-none');

        for (var i = count - 1; i >= 0; i--) {
          // move the last item to dropdown
          $(child[i]).prependTo(overflowList);

          // recalc width
          thisWidth = $list.outerWidth();
          if (thisWidth < maxWidth) break;
        }
      }

      function refresh() {
        thisWidth = $list.outerWidth();
        maxWidth = $(container).width() - $(container).find('.all').width();
        var docWidth = $(document).width();

        if ($(window).outerWidth() > settings.breakPoint) {
          reset();
        } else if (thisWidth > maxWidth) {
          shrink();
        } else if (0 < (docWidth - lastDocWidth)) {
          grow();
        }
        lastDocWidth = docWidth;
      }

      $(window).on('resize load', function () {
        refresh();
      });
    });
  }
}(jQuery));



$(function () {

  $('.brand-nav .pagination').responsiveCollapse();

  $('.slider.slider-news').on('init', function (e) {
    $('.card--news').matchHeight();
  });

  $('.slider').on('breakpoint', function (e) {
    $('.card--news').matchHeight();
  });

  $('.slider.slider-news').slick({
    //slide: 'div',
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {

        breakpoint: 576,
        settings: {
          arrows: false,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });

  $('.slider.slider-brands').slick({
    //slide: 'div',
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {

        breakpoint: 576,
        settings: {
          arrows: false,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }

    ]
  });

  $('.slider.slider-look').slick({
    //slide: 'div',
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 576,
        settings: {
          arrows: false,
          slidesToShow: 2,
          slidesToScroll: 1
        }
      }
    ]
  });


//NEWS
  $('.news-items .card--news').matchHeight();

  function getPenPath() {
    if (this.loadCount < 4) {
      return 'news/' + this.loadCount + '.html';
    }
  }

  var $newsContainer = $('.news-items .row');

  //infinite scroll on news page
  $newsContainer.infiniteScroll({
    path: getPenPath,
    append: '.col-xs-12.col-sm-12.col-md-6.col-lg-4',
    status: '.scroller-status',
    history: false,
  });


  $newsContainer.on('append.infiniteScroll', function (event, response, path, items) {
    //$('.news-items .card--news').matchHeight();
  });

});


//DEMO
$(function () {
  $('.newsletter button[type=submit]').on('click', function (e) {
    e.preventDefault();
    $(this).closest('.container').hide();
    $(this).closest('.newsletter').find('.newsletter__done').show();
  });

  $('.newsletter .newsletter__close').on('click', function (e) {
    e.preventDefault();
    $(this).closest('.newsletter').hide();
  });
});
