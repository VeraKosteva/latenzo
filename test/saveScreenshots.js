const puppeteer = require('puppeteer');
const FileHound = require('filehound');
const shell = require('shelljs');

const config = {
  outputPath: "snapshots/",
  resolution: [360, 720, 1200]
};

const files = FileHound.create()
  .paths('../app')
  .ext('html')
  .depth(0)
  .find().then(getScreen);

/*

files.then(function (e) {
  console.log('files');
  console.log(e)
})
;
*/


async function getScreen(fileList, a, b, c) {
  console.log(fileList, b, c);

  const browser = await puppeteer.launch();

  const page = await browser.newPage();

  for (let j = 0; j < config.resolution.length; j++) {
    let resolution = config.resolution[j];
    let basePath = config.outputPath + resolution + '/';
    shell.mkdir('-p', basePath);


    await page.setViewport({
      width: resolution,
      height: 1080
    });

    for (let i = 0; i < fileList.length; i++) {
      let filename = fileList[i].replace(/^.*[\\\/]/, '')
      console.info("Take", filename,"on",resolution);

      let name = filename.split('.')[0];
      await page.goto('http://localhost:9000/' + filename);
      await page.screenshot({path: basePath + name + '.png', fullPage: true});
    }
  }
  await browser.close();
}

/*
(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
    await page.setViewport({
  	width: 1920,
  	height: 1080
  });
  await page.goto('http://localhost:9000/theme.html');
  await page.screenshot({path: 'theme.png', fullPage :true});

  await page.goto('http://localhost:9000/index.html');
  await page.screenshot({path: 'index.png', fullPage :true});

  await browser.close();
})();
*/
