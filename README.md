# Latenzo

HTML / CSS for Latenzo

#### Getting Started ####

* Install global dependencies: `npm install --global gulp-cli bower`
* Install dependencies: `npm i && bower i`
* Run `gulp serve` to preview and watch for changes
* Run `gulp` to build all HTML/SCS, result files will be in `dist` folder
* Run `gulp serve:dist` to preview the production build


#### GEMMINI ####
* https://github.com/gemini-testing/gemini

##### Installing

```
npm install -g gemini
npm install -g selenium-standalone
selenium-standalone install
```

##### Running tests
Start `selenium-standalone` in a separate tab before running the tests:
```
selenium-standalone start
```

Updating reference images

Once you have few suites written or some code changed you need to capture or update reference images:
```
gemini update
```

Run gemini tests with flat reporters:
```
gemini test --reporter flat
```

##### Gemini GUI

Ro be able to use GUI on a project you must have gemini installed locally in this project.

```
npm i -g gemini-gui
```

Run in the project root:
```
gemini-gui
```

#### GALEN ####

```
galen test test\screen.test
```
